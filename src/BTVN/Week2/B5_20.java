/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week2;

/**
 *
 * @author KING
 */
public class B5_20 {
    public static double FindPi(int n)
    {
        int odd=1;
        double Pi=0;
        for(int i=0;i<n;++i)
        {
            if(i%2==0) Pi=Pi+(double)4/odd;
            else Pi=Pi-(double)4/odd;        
            odd=odd+2;        
        }
        return Pi;
    }
    public static void main(String[] args) {
        for(int i=0;i<200000;i++)
        {System.out.print(i);System.out.print(" : ");System.out.println(FindPi(i));}
    }
}
