/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week2;
import java.util.Scanner;
/**
 *
 * @author KING
 */
public class B4_38 {  
    public static int encryptor(int a)
    {
        int Num[] =new int [4];
        for(int i=0;i<4;i++)
        {
            Num[3-i]=a%10;
            a=a/10;
        }
        int temp;
        temp=Num[1];
        Num[1]=Num[3];
        Num[3]=temp;
        temp=Num[2];
        Num[2]=Num[0];
        Num[0]=temp;
        for(int i=0;i<4;i++)
            Num[i]=(Num[i]+7)%10;
        a=0;
        for(int i=0;i<4;i++)
            a=a*10+Num[i];
        return a;
    }
    public static int decryptor(int a)
    {
        int Num[] =new int [4];
        for(int i=0;i<4;i++)
        {
            Num[3-i]=a%10;
            a=a/10;
        }
        int temp;
        temp=Num[1];
        Num[1]=Num[3];
        Num[3]=temp;
        temp=Num[2];
        Num[2]=Num[0];
        Num[0]=temp;
        for(int i=0;i<4;i++)
        {
            Num[i]=Num[i]-7;
            if(Num[i]<0) Num[i]=Num[i]+10;
        }
        a=0;
        for(int i=0;i<4;i++)
            a=a*10+Num[i];
        return a;
    }
    
    public static void main(String[] args) {
        int a=new Scanner(System.in).nextInt();
        //int a=2345;
        System.out.println(encryptor(a));
        System.out.println(decryptor(encryptor(a)));
    }
}
