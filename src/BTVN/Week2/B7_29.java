/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week2;

import java.util.Scanner;

/**
 *
 * @author KING
 */
public class B7_29 {
    public static int fibonaci(int n)
    {
        if(n==1) return 0;
        if(n==2) return 1;
        return (fibonaci(n-1)+fibonaci(n-2));
    }
    public static void MaxDisplayInt()
    {
        int count = 2;
        while(fibonaci(count)>0) count++;
        System.out.println(count-1);
    }
    
    public static double fibonaciDouble(int n)
    {
        if(n==1) return 0;
        if(n==2) return 1;
        return (fibonaciDouble(n-1)+fibonaciDouble(n-2));
    }
    public static void MaxDisplayDouble()
    {
        int count = 2;
        while(fibonaciDouble(count)<=1.79769e+308) count++;
        System.out.println(count-1);
    }
    public static void MakeFibonaci(int f[],int n)
    {
        f[0]=0;f[1]=1;
        for(int i=2;i<n;i++)
            f[i]=f[i-1]+f[i-2];
    }
    public static void MakeFibonaci(double f[],int n)
    {
        f[0]=0;f[1]=1;
        for(int i=2;i<n;i++)
            f[i]=f[i-1]+f[i-2];
    }
    
    public static void main(String [] args)
    {
        //n>0
        int n=new Scanner(System.in).nextInt();
        System.out.println(fibonaci(n));
        
        //MaxDisplayInt();
        int FiTable[] =new int[50];
        MakeFibonaci(FiTable,50);
        int count=0;
        while(FiTable[count]>=0) count++;
        System.out.println(FiTable[count-1]);
        
        MaxDisplayDouble();
        /*
        /Khong the dung mang vi se tran bo nho
        */
//        double FiTableDouble[] =new double[1000000000];
//        MakeFibonaci(FiTableDouble,1000000000);
//        count=0;
//        while(FiTableDouble[count]<=1.79769e+308) count++;
//        System.out.println(FiTableDouble[count-1]);
        
    }
    
}
