/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week2;
import java.util.Scanner;
/**
 *
 * @author KING
 */

public class B4_37 {
    public static float Factorial(int n)
    {
        if(n<0) return 0;
        if(n<2) return 1;
        float r=1;
        for(int i=1;i<=n;i++) r*=i;
        return r;
    }
    public static float e(int n)
    {
        float e=1;
        for(int i=1;i<n;i++)
        {
            e+=1/Factorial(i);
        }
        return e;
    }
    public static float ex(int x,int n)
    {
        float e=1;
        for(int i=1;i<n;i++)
        {
            e+=Math.pow(x,i)/Factorial(i);
        }
        return e;
    }
    
    public static void main(String[] args) {
        int n=new Scanner(System.in).nextInt();
        System.out.printf("%d%s%f\n",n,"! = ",Factorial(n));
        System.out.println(e(n));
        int x=new Scanner(System.in).nextInt();
        System.out.println(ex(x,n));
    }
}
