/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B8_15;

/**
 *
 * @author KING
 */
public class Rational {
    private int numerator;
    private int denominator;
    
    public final int ucln(int a,int b)
    {
        while (b!=0)
        {
            if (b>a) {a=a+b;b=a-b;a=a-b;}
            if (b==0)
                break;
            else a=a%b;
        }
        return a;
    }    

    public Rational(int numerator, int denominator) {
        this.numerator = numerator/ucln(numerator,denominator);
        this.denominator = denominator/ucln(numerator,denominator);
    }
    
    public Rational add(Rational rational1,Rational rational2)
    {
        return new Rational(rational1.numerator*rational2.denominator+rational2.numerator*rational1.denominator,rational1.denominator*rational2.denominator);
    }
    public Rational subtract(Rational rational1,Rational rational2)
    {
        return new Rational(rational1.numerator*rational2.denominator-rational2.numerator*rational1.denominator,rational1.denominator*rational2.denominator);
    }
    public Rational multiply(Rational rational1,Rational rational2)
    {
        return new Rational(rational1.numerator*rational2.numerator,rational1.denominator*rational2.denominator);
    }
    public Rational divide(Rational rational1,Rational rational2)
    {
        return new Rational(rational1.numerator*rational2.denominator,rational1.denominator*rational2.numerator);
    }
    
    public String stringRational()
    {
        return(String.valueOf(numerator)+"/"+String.valueOf(denominator));    
    }
    public String stringRationalFloatingPoint()
    {
        return(String.valueOf((float)numerator/denominator));
    }
    
    public static void main(String[] args) {
        Rational rational1=new Rational(2,4);
        Rational rational2=new Rational(2,3);
        System.out.println(rational1.stringRational());
        System.out.println(rational1.stringRationalFloatingPoint());
        rational1=rational1.add(rational1, rational2);
        System.out.println(rational1.stringRational());
        rational1=rational1.divide(rational1, rational2);
        System.out.println(rational1.stringRational());
        rational1=rational1.multiply(rational1, rational2);
        System.out.println(rational1.stringRational());
        rational1=rational1.subtract(rational1, rational2);
        System.out.println(rational1.stringRational());
    }
}
