/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B8_11;

/**
 *
 * @author KING
 */
public class ComplexNumbers {
    private int realPart;
    private int imaginaryPart;

    public ComplexNumbers(int realPart, int imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public int getRealPart() {
        return realPart;
    }

    public int getImaginaryPart() {
        return imaginaryPart;
    }

    public void setRealPart(int realPart) {
        this.realPart = realPart;
    }

    public void setImaginaryPart(int imaginaryPart) {
        this.imaginaryPart = imaginaryPart;
    }
    
    public ComplexNumbers add(ComplexNumbers complexNumber1,ComplexNumbers complexNumber2)
    {
        return new ComplexNumbers(complexNumber1.getRealPart()+complexNumber2.getRealPart(),complexNumber1.getImaginaryPart()+complexNumber2.getImaginaryPart());
    }
    public ComplexNumbers subtract(ComplexNumbers complexNumber1,ComplexNumbers complexNumber2)
    {
        return new ComplexNumbers(complexNumber1.getRealPart()-complexNumber2.getRealPart(),complexNumber1.getImaginaryPart()-complexNumber2.getImaginaryPart());
    }
    public void display()
    {
        System.out.printf("%s%d\n%s%d\n","Phan Thuc : ",getRealPart(),"Phan Ao : ",getImaginaryPart());
    }
    public static void main(String[] args) {
        ComplexNumbers complexNumber1=new ComplexNumbers(1,3);
        ComplexNumbers complexNumber2=new ComplexNumbers(2,4);
        ComplexNumbers sum=complexNumber1.add(complexNumber1,complexNumber2);
        ComplexNumbers difference=complexNumber1.subtract(complexNumber1,complexNumber2);
        complexNumber1.display();
        complexNumber2.display();
        sum.display();
        difference.display();
    }
}
