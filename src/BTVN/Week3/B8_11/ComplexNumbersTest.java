/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B8_11;

/**
 *
 * @author KING
 */
public class ComplexNumbersTest {
    public static void main(String[] args) {
        ComplexNumbers complexNumber1=new ComplexNumbers(1,3);
        ComplexNumbers complexNumber2=new ComplexNumbers(2,4);
        ComplexNumbers sum=complexNumber1.add(complexNumber1,complexNumber2);
        ComplexNumbers difference=complexNumber1.subtract(complexNumber1,complexNumber2);
        complexNumber1.display();
        complexNumber2.display();
        sum.display();
        difference.display();
    }
    
}
