/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B3_15;

/**
 *
 * @author KING
 */
public class Date {
    private int date;
    private int month;
    private int year;

    public Date(int month, int date, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }

    public int getDate() {
        return date;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    public void display()
    {
        System.out.printf("%d%s%d%s%d\n",getMonth(),"/",getDate(),"/",getYear());
    }
}
