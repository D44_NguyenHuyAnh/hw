/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B4_13;

/**
 *
 * @author KING
 */
public class IntegerSet {
    private boolean[] intSet = new boolean[100];

    public IntegerSet() {
        this.intSet = new boolean[100];
    }

    public boolean[] getIntSet() {
        return intSet;
    }
    
    public void insertElement(int i)
    {
        if((i<100)&&(i>=0)&&intSet[i]==false) intSet[i]=true;
    }
    public void deleteElement(int i)
    {
        if((i<100)&&(i>=0)&&intSet[i]==true) intSet[i]=false;
    }
    public IntegerSet union(IntegerSet intSet1,IntegerSet intSet2)
    {
        IntegerSet temp=new IntegerSet();
        for(int i=0;i<100;i++)
            if((intSet1.intSet[i]==true)||(intSet2.intSet[i]==true)) 
                temp.intSet[i]=true;
        return temp;
    }
    public IntegerSet intersection(IntegerSet intSet1,IntegerSet intSet2)
    {
        IntegerSet temp=new IntegerSet();
        for(int i=0;i<100;i++)
            if((intSet1.intSet[i]==true)&&(intSet2.intSet[i]==true)) 
                temp.intSet[i]=true;
        return temp;
    }
    @Override
    public String toString()
    {
        String s="";
        for(int i=0;i<100;i++)
            if(intSet[i]==true) s=s+String.valueOf(i)+' ';
        if(s.equals("")) s="---";
        return s;
    }
}
