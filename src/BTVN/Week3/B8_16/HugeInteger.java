/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B8_16;

public class HugeInteger {
    private int [] integerDigits;
    private int numberDigits;
    
    //Neu tran thi tao gia tri bang 0
    public HugeInteger(String stringOfIntegers)
    {
        this.integerDigits = new int[40];
        this.numberDigits = 0;
        this.integerDigits = parseFunction(stringOfIntegers);
    }

    public int[] parseFunction (String str)
    {
        boolean ck=false;
        if(str.charAt(0)=='-')
        {
            ck=true;
            str=str.substring(1);
        }
        if(str.charAt(0)=='+')
        {
            str=str.substring(1);
        }
        while((str.charAt(0)=='0')&&(str.length()>1)) str=str.substring(1);
        if(str.length()>40)
        {
            System.out.printf("%s\n","Tran!!!!!!!!!");
            this.numberDigits=1;
            return integerDigits;
        }
        else
        {
            for (int i = 0; i < str.length(); i++)
            {
                char ch = str.charAt(i);
                if (Character.isDigit(ch))
                    if(ck) integerDigits[i] = -(Character.getNumericValue(ch));
                    else integerDigits[i] = Character.getNumericValue(ch);
                else integerDigits[i] = 0;
            }
            this.numberDigits=str.length();
        }   
        return integerDigits;
    }
    
    public String toStringFunction()
    {
        String temp="";
        for(int i=0;i<numberDigits;i++)
            if(integerDigits[0]>0)
                temp=temp+(char)(integerDigits[i]+48);
            else temp=temp+(char)((-integerDigits[i])+48);    
        if(integerDigits[0]>=0) return temp;else return "-"+temp;
    }

    public HugeInteger addFunction(HugeInteger number1,HugeInteger number2)
    {
        if(number1.numberDigits==0)return number2;
        if(number2.numberDigits==0)return number1;
        
        if(number2.numberDigits>number1.numberDigits)
        {
            HugeInteger temp=number1;
            number1=number2;
            number2=temp;
        }
        
        int dif=number1.numberDigits-number2.numberDigits;
        if(dif>0)
        {
            for(int i=0;i<number2.numberDigits;i++)
                number2.integerDigits[number1.numberDigits-i-1]=number2.integerDigits[number2.numberDigits-i-1];
            for(int i=0;i<dif;i++)
                number2.integerDigits[i]=0;
        }
        
        int carry=0;
        for(int i=number1.numberDigits-1;i>=0;i--)
        {
            number1.integerDigits[i]=(number1.integerDigits[i]+number2.integerDigits[i]+carry);
            carry = number1.integerDigits[i]/10;
            number1.integerDigits[i] = number1.integerDigits[i]%10;
        }
        
        if(carry!=0) 
            if(number1.numberDigits==40)
            {
                System.out.printf("%s\n","Tran!!!!!!!!!");
                return new HugeInteger("0");
            }
            else 
            {
                for(int i=1;i<number1.numberDigits;i++)
                    number1.integerDigits[number1.numberDigits-i]=number1.integerDigits[number1.numberDigits-i-1];
                number1.integerDigits[0]=carry;
            }
        
        int count=0;
        while((number1.integerDigits[count]==0)&&count<39) count++;
        if(count==40) return new HugeInteger("0");
        number1.numberDigits=number1.numberDigits-count;
        for(int i=0;i<number1.numberDigits;i++)
            number1.integerDigits[i]=number1.integerDigits[i+count];

        return number1;
    }
    
    public HugeInteger subtractFunction(HugeInteger number1,HugeInteger number2)
    {
        for(int i=0;i<40;i++)
            number2.integerDigits[i]=-number2.integerDigits[i];
        return(addFunction(number1,number2));
    }
    
    public boolean isEqualsFunction(HugeInteger number)
    {
        if(numberDigits!=number.numberDigits) return false;
        for(int i=0;i<number.numberDigits;i++)
            if(integerDigits[i]!=number.integerDigits[i])
            {
                return false;
            }
        return true;
    }
    public boolean isGreaterThan(HugeInteger number)
    {
        if(numberDigits>number.numberDigits) return true;
        if(numberDigits<number.numberDigits) return false;
        if(isEqualsFunction(number)) return false;
        for(int i=0;i<numberDigits;i++)
            if(integerDigits[i]<number.integerDigits[i])
                return false;
        return true;
    }
    public boolean notEqualsFunction(HugeInteger number)
    {
        return (!isEqualsFunction(number));
    }
    public boolean isGreaterOrEqualTo(HugeInteger number)
    {
        return(isGreaterThan(number)||isEqualsFunction(number));
    }
    public boolean isLessThan(HugeInteger number)
    {
        return (!isGreaterOrEqualTo(number));
    }
    
    public void display()
    {
            System.out.printf("%s\n",toStringFunction());
    }
    
    public static void main(String[] args) {
        HugeInteger number1=new HugeInteger("1120");
        HugeInteger number2=new HugeInteger("1121");
        number1.display();
        number2.display();
        HugeInteger number=new HugeInteger("-0000000000000000000000000000000000000000000000000000000000000000000000000321");
        number.display();
        number1.addFunction(number1,number2).display();
        number1.subtractFunction(number1,number2).display();
        number1.subtractFunction(number2,number1).display();
        System.out.printf("%b\n",number1.isEqualsFunction(number2));
        System.out.printf("%b\n",number1.notEqualsFunction(number2));
        System.out.printf("%b\n",number1.isGreaterThan(number2));
        System.out.printf("%b\n",number1.isGreaterOrEqualTo(number2));
        System.out.printf("%b\n",number1.isLessThan(number2));
    }
}
