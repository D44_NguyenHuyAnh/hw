/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week3.B8_14;

/**
 *
 * @author KING
 */
public class Date {
    private int month;
    private int date;
    private int year;

    public Date(int month, int date, int year) {
        this.month = month;
        this.date = date;
        this.year = year;
    }

    public int convertStringMonth(String month)
    {
        int monthInt=0;
        switch(month)
        {
            case"January":monthInt = 1;break;
            case"February":monthInt = 2;break;
            case"March":monthInt = 3;break;
            case"April":monthInt = 4;break;
            case"May":monthInt = 5;break;
            case"June":monthInt = 6;break;
            case"July":monthInt = 7;break;
            case"August":monthInt = 8;break;
            case"September":monthInt = 9;break;
            case"October":monthInt = 10;break;
            case"November":monthInt = 11;break;
            case"December":monthInt = 12;break;
            default: break;
        }
        return monthInt;
    }
    public Date(String dateString)
    {
        String temp[] = dateString.split(" ");
        int tempMonth;
        int tempDate;
        int tempYear;
        tempMonth=convertStringMonth(temp[0]);
        tempDate=Integer.parseInt(temp[1].substring(0,2));
        tempYear=Integer.parseInt(temp[2]);
        this.month=tempMonth;
        this.date=tempDate;
        this.year=tempYear;
    }
    
    public Date(int date, int year) {
        this.date = date;
        this.year = year;
    }
    
    public void display()
    {
        System.out.printf("%d%s%d%s%d",month,"/",date,"/",year);
    }
}
