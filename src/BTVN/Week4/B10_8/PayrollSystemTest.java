/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_8;

/**
 *
 * @author KING
 */
public class PayrollSystemTest {
    public static void main( String[] args ){
        Date date1=new Date(1,23,1990);
        Date date2=new Date(2,13,1960);
        Date date3=new Date(6,9,1986);
        Date date4=new Date(10,9,1976);
        SalariedEmployee salariedEmployee = new SalariedEmployee( "John", "Smith", "111-11-1111", 800.00,date1);
        HourlyEmployee hourlyEmployee = new HourlyEmployee( "Karen", "Price", "222-22-2222", 16.75, 40 ,date2); 
        CommissionEmployee commissionEmployee = new CommissionEmployee( "Sue", "Jones", "333-33-3333", 10000, .06 ,date3);
        BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee( "Bob", "Lewis", "444-44-4444", 5000, .04, 300 ,date4);
        
        System.out.println( "Employees processed individually:\n" );
        System.out.printf( "%s\n%s: $%,.2f\n\n", salariedEmployee, "earned", salariedEmployee.earnings() );
        System.out.printf( "%s\n%s: $%,.2f\n\n", hourlyEmployee, "earned", hourlyEmployee.earnings() );
        System.out.printf( "%s\n%s: $%,.2f\n\n", commissionEmployee, "earned", commissionEmployee.earnings() );
        System.out.printf( "%s\n%s: $%,.2f\n\n", basePlusCommissionEmployee, "earned", basePlusCommissionEmployee.earnings() ); 
        
        Employee[] employees = new Employee[4];
        employees[ 0 ] = salariedEmployee;
        employees[ 1 ] = hourlyEmployee;
        employees[ 2 ] = commissionEmployee;
        employees[ 3 ] = basePlusCommissionEmployee;
        
        for ( Employee currentEmployee : employees )
            if (currentEmployee instanceof BasePlusCommissionEmployee)
            {
                BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;
                employee.setBaseSalary( 1.10 * employee.getBaseSalary() );
            }
        
        System.out.println( "Employees processed polymorphically:\n" );
        for(int i=1;i<=12;i++)
        {    
            System.out.printf("Salary month %d\n",i);
            for ( Employee currentEmployee : employees )
            {
                System.out.println( );
                if (currentEmployee.getBirthDay().getMonth()==i) 
                    System.out.printf("earned with birth day increase $%,.2f\n\n",100.0+currentEmployee.earnings()); 
                else System.out.printf("earned $%,.2f\n\n",currentEmployee.earnings());    
            }
        }
        
    }
    
}
