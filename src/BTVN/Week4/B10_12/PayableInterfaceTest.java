/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_12;

/**
 *
 * @author KING
 */
public class PayableInterfaceTest {
    public static void main(String[] args) {
        
        SalariedEmployee salariedEmployee = new SalariedEmployee( "John", "Smith", "111-11-1111", 800.00 );
        HourlyEmployee hourlyEmployee = new HourlyEmployee( "Karen", "Price", "222-22-2222", 16.75, 40 ); 
        CommissionEmployee commissionEmployee = new CommissionEmployee( "Sue", "Jones", "333-33-3333", 10000, .06 );
        BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee( "Bob", "Lewis", "444-44-4444", 5000, .04, 300 );
        
        Payable[] payableObjects = new Payable[ 4 ];
        
        payableObjects[ 0 ]=salariedEmployee;
        payableObjects[ 1 ]=hourlyEmployee;
        payableObjects[ 2 ]=commissionEmployee;
        payableObjects[ 3 ]=basePlusCommissionEmployee; 

        System.out.println( "Employees processed polymorphically:\n" );
        for (Payable currentPayableObjects : payableObjects)
        {
            if (currentPayableObjects instanceof BasePlusCommissionEmployee)
            {
                 BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentPayableObjects;
                 employee.setBaseSalary( 1.10 * employee.getBaseSalary() );
                 System.out.printf("new base salary with 10%% increase is: $%,.2f\n",employee.getBaseSalary() ); 
            }
            System.out.printf("earned $%,.2f\n\n",currentPayableObjects.getPaymentAmount());    
        }
    }
}
