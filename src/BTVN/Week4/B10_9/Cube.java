/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 *
 * @author KING
 */
public class Cube extends ThreeDimensionalShape{
    private int edge;

    public Cube(int edge) {
        this.edge = edge;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return(6*edge*edge);
    }

    @Override
    public double getVolume() {
        return Math.pow(edge,3);
    }
    
    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n%s%f\n","Hinh Lap Phuong co:","    Canh : ",edge,"    Dien Tich Be Mat : ",getArea(),"    The Tich:",getVolume());
    }
    
    
}
