/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 * Gia su tu dien deu
 * @author KING
 */
public class Tetrahedron extends ThreeDimensionalShape{
    private int edge;

    public Tetrahedron(int edge) {
        this.edge = edge;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return((Math.sqrt(3)*edge)/4);
    }

    @Override
    public double getVolume() {
        return((Math.sqrt(2)*Math.pow(edge,3))/12);
    }
   
    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n%s%f\n","Hinh Tu Dien Deu co:","    Canh : ",edge,"    Dien Tich Be Mat : ",getArea(),"    The Tich:",getVolume());
    }
    
     
}
