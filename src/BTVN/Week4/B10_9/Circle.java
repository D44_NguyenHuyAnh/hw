/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 *
 * @author KING
 */
public class Circle extends TwoDimensionalShape{   
    private int radius;
 
    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    
    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n","Hinh Tron co:","    Ban Kinh : ",radius,"    Dien Tich : ",getArea());
    }
    
}
