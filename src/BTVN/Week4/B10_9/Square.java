/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 *
 * @author KING
 */
public class Square extends TwoDimensionalShape{
    private int edge;

    public Square(int edge) {
        this.edge = edge;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return (edge*edge);
    }
    
    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n","Hinh Vuong co:","    Canh : ",edge,"    Dien Tich : ",getArea());
    }
    
}
