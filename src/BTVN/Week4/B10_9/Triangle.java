/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;
/**
 * Gia su tam giac deu
 * @author KING
 */
public class Triangle extends TwoDimensionalShape{
    private int edge;

    public Triangle(int edge) {
        this.edge = edge;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int edge) {
        this.edge = edge;
    }
    
    @Override
    public double getArea() {
        return((Math.sqrt(3)*edge*edge)/4);
    }

    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n","Hinh Tam Gic Deu co:","    Canh : ",edge,"    Dien Tich : ",getArea());
    }
    
    
}
