/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 *
 * @author KING
 */
public class Sphere extends ThreeDimensionalShape{
    private int radius;

    public Sphere(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return (4*Math.PI*radius*radius);
    }

    @Override
    public double getVolume() {
        return ((4*Math.PI*Math.pow(radius,3))/3);
    }
       
    @Override
    public void Display() {
        System.out.printf("%s\n%s%d\n%s%f\n%s%f\n","Hinh Cau co:","    Ban Kinh : ",radius,"    Dien Tich Be Mat : ",getArea(),"    The Tich:",getVolume());
    }
    
 
    
    
}
