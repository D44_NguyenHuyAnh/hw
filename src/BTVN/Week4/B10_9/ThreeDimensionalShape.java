/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_9;

/**
 *
 * @author KING
 */
abstract public class ThreeDimensionalShape extends Shape{
    abstract public double getArea();
    abstract public double getVolume();
}
