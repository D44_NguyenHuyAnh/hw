/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_10;

/**
 *
 * @author KING
 */
public class PieceWorker extends Employee {
    private int piece;
    private double wage;

    public PieceWorker( String first, String last, String ssn,int piece, double wage) {
        super(first, last, ssn);
        this.piece = piece;
        this.wage = wage;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    @Override
    public double earnings() {
        return (wage*piece);
    }
    
    @Override 
    public String toString() 
    { 
        return String.format( "hourly employee: %s\n%s: $%,.2f; %s: %d", super.toString(), "wage per piece", getWage(), "number of pieces", getPiece() );
    }
}
