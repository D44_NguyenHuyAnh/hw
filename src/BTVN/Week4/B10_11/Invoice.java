/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B10_11;

/**
 *
 * @author KING
 */
public class Invoice implements Payable {
    private String partNumber;
    private String partDescription;
    private int quantity;
    private double pricePerItem;
    
    public Invoice( String part, String description, int count, double price )
    {
        partNumber = part;
        partDescription = description;
        setQuantity( count );
        setPricePerItem( price );
    }
    
    public void setQuantity( int count )
    {
        if ( count >= 0 ) quantity = count;
        else throw new IllegalArgumentException( "Quantity must be >= 0" );
    }
    
    public void setPricePerItem( double price )
    {
        if ( price >= 0.0 ) pricePerItem = price; 
        else throw new IllegalArgumentException( "Price per item must be >= 0" );
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPricePerItem() {
        return pricePerItem;
    }
    
    @Override 
    public String toString()
    {
        return String.format( "%s: \n%s: %s (%s) \n%s: %d \n%s: $%,.2f", "invoice", "part number", getPartNumber(), getPartDescription(), "quantity", getQuantity(), "price per item", getPricePerItem() );
    } 
     
    @Override public double getPaymentAmount() 
    { 
        return getQuantity() * getPricePerItem();
    }
    
}
