/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN.Week4.B9_8;

/**
 *
 * @author KING
 */
public class Quadrilateral {
    private Point point1;
    private Point point2;
    private Point point3;
    private Point point4;

    public Quadrilateral(Point point1, Point point2, Point point3, Point point4) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.point4 = point4;
    }
    
    public void display(){
        point1.Display();
        point2.Display();
        point3.Display();
        point4.Display();
    }
    
    private double getAreaTriangle(Point point1,Point point2,Point point3)
    {
        return (0.5*Math.abs((point3.getX()-point1.getX())*(point2.getY()-point1.getY())-(point2.getX()-point1.getX())*(point3.getY()-point1.getY())));
    }
    
    public double getArea()
    {
        if(point1.getX()==point3.getX())
        {
            if((point2.getX()>point1.getX())&&(point4.getX()<point1.getX()))
                    return (getAreaTriangle(point1,point2,point3)+getAreaTriangle(point1,point4,point3));
            return(getAreaTriangle(point1,point4,point3)+getAreaTriangle(point2,point4,point3));
        }
        
        double a=(double)(point1.getY()-point3.getY())/(point1.getX()-point3.getX());
        double b=(double)point3.getY()-a*point1.getX();
        if(((a*point1.getX()+b-point1.getY())*(a*point3.getX()+b-point3.getY()))<0)
            return (getAreaTriangle(point1,point2,point3)+getAreaTriangle(point1,point4,point3));
        return(getAreaTriangle(point1,point4,point3)+getAreaTriangle(point2,point4,point3));        
    }
}
