/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BTVN;
import java.util.Scanner;
/**
 *
 * @author KING
 */
public class BTVNTuan1 {
    public static int Max3Int(int a,int b,int c)
    {
        if(b>a) a=b;
        if(c>a) a=c;
        return a;        
    }
    public static int Min3Int(int a,int b,int c)
    {
        if(b<a) a=b;
        if(c<a) a=c;
        return a;        
    }
    public static int Max5Int(int a,int b,int c,int d,int e)
    {
        if(b>a) a=b;
        if(c>a) a=c;
        if(d>a) a=d;
        if(e>a) a=e;
        return a;        
    }
    public static int Min5Int(int a,int b,int c,int d,int e)
    {
        if(b<a) a=b;
        if(c<a) a=c;
        if(d<a) a=d;
        if(e<a) a=e;
        return a;        
    }
    public static int StrToNum(String s)
    {
        int a=0;
        while(s != "")
        {
            for(int i=0;i<s.length();i++)
            {
                if(s.charAt(0) != ' ') a=a*10+(int)s.charAt(0)-48;
                if(s.length()==1) s=""; else s=s.substring(1);
            }
        }
        return a;
    }
    
    public static void main(String[] args) {
        int a,b,c,d,e;
        float Pi=(float) 3.14159;
        String s;
        
        System.out.println("Bai 2.17 : Nhap 3 so :");
        a=new Scanner(System.in).nextInt();
        b=new Scanner(System.in).nextInt();
        c=new Scanner(System.in).nextInt();
        System.out.printf("%s%d\n","    Tong : ",a+b+c);
        System.out.printf("%s%d\n","    Tich : ",a*b*c);
        System.out.printf("%s%d\n","    Trung Binh ( ket qua so nguyen ) : ",(a+b+c)/3);
        System.out.printf("%s%f\n","    Trung Binh : ",(float)(a+b+c)/3);
        System.out.printf("%s%d\n","    Lon Nhat : ",Max3Int(a,b,c));
        System.out.printf("%s%d\n","    Nho Nhat : ",Min3Int(a,b,c));
        
        System.out.println("Bai 2.18 : ");
        System.out.println("*******     ***      *        *");
        System.out.println("*     *    *   *    ***      * *");
        System.out.println("*     *   *     *  *****    *   *");    
        System.out.println("*     *    *   *     *       * *");
        System.out.println("*******     ***      *        *");
        
        System.out.println("Bai 2.19 : Dong lenh se in ra man hinh : \n*\n**\n***\n****\n*****" );
        
        System.out.println("Bai 2.23 : Dong lenh se in ra man hinh : \n*\n***\n*****\n" );
        
        System.out.println("Bai 2.24 : Nhap 5 so :");
        a=new Scanner(System.in).nextInt();
        b=new Scanner(System.in).nextInt();
        c=new Scanner(System.in).nextInt();
        d=new Scanner(System.in).nextInt();
        e=new Scanner(System.in).nextInt();
        System.out.printf("%s%d\n","    Lon Nhat : ",Max5Int(a,b,c,d,e));
        System.out.printf("%s%d\n","    Nho Nhat : ",Min5Int(a,b,c,d,e));
        
        System.out.println("Bai 2.28 : Nhap ban kinh: ");
        int r=new Scanner(System.in).nextInt();
        System.out.printf("%s%d\n","    Duong kinh : ",2*r);
        System.out.printf("%s%f\n","    Chu vi : ",2*r*Pi);
        System.out.printf("%s%f\n","    Duong kinh : ",Pi*r*r);
        
        System.out.println("Bai 2.29 : ");
        s="ABCabc012$*+/";
        for(int i=0;i<s.length();i++)
            System.out.printf("%s%c%s%d\n","    ",s.charAt(i)," : ",(int)s.charAt(i));
        
        System.out.println("Bai 2.30 : Nhap du lieu :");
        s=new Scanner(System.in).nextLine();
        System.out.printf("%s%d\n","Ket qua : ",StrToNum(s));
        
        System.out.println("Bai 2.31 : ");
            for(int i=0;i<=10;i++)
                System.out.printf("%s%3d%s%3d%s%3d\n","    ",i," ",i*i," ",i*i*i);
            
        System.out.println("Bai 2.32 : Nhap 5 so :");
        int Num[]=new int[5];
        for(int i=0;i<5;i++) Num[i]=new Scanner(System.in).nextInt();
        int neg = 0;
        int pos = 0;
        for(int i=0;i<5;i++) 
        {
            if(Num[i]>0) pos++;
            if(Num[i]<0) neg++;
        }
        System.out.printf("%s%d","    So so duong : ",pos);
        System.out.printf("%s%d","    So so am : ",neg);
        System.out.printf("%s%d","    So so bang 0 : ",5-(pos+neg));
        
        
    }
}
