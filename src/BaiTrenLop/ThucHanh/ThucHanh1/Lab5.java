/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.ThucHanh.ThucHanh1;

/**
 *
 * @author KING
 */
import java.util.Scanner;
public class Lab5 {
    public static String ChuanHoa(String s)
    {
        s=s.trim();
        if(s=="") return"";
        if(s.length()==1) return s;
        s=s.replaceAll("\\s+"," ");
        s=s.toLowerCase();
        String temp[] = s.split(" ");
        s = "";
        for (int i = 0; i < temp.length; i++) 
        {
            s += String.valueOf(temp[i].charAt(0)).toUpperCase() + temp[i].substring(1);
            if (i < temp.length-1) s+=" ";
        }
        
        return s;
    }
    public static String DaoChuoi(String s)
    {

        String temp = "";
        for(int i=s.length()-1;i>=0;i--) temp+=s.charAt(i);
        return temp;
    }
    public static void DoiXung(String s)
    {
        if(s.equals(DaoChuoi(s))) System.out.println("Doi Xung"); 
        else System.out.println("Khong Doi Xung");
    }
    
    public static void main(String[] args)
    {
        String s;
        s= new Scanner(System.in).nextLine();
        System.out.println(s);
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
        System.out.println(s);
        DoiXung(s);
        System.out.println(DaoChuoi(s));
        System.out.println(ChuanHoa(s));

    
    }
    
}
