/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan4;

/**
 *
 * @author KING
 */
public class Cow {
    private String sound;

    public Cow(String sound) {
        this.sound = sound;
    }
    
    public void makeASound()
    {
        System.out.printf("I say %s\n",sound);
    }
}
