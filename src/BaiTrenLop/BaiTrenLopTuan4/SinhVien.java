/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan4;

/**
 *
 * @author KING
 */
public class SinhVien extends Nguoi{
    private float diem;

    public SinhVien() {
    }

    public SinhVien(String hoTen, boolean gioiTinh, String queQuan, float diem) {
        super(hoTen, gioiTinh, queQuan);
        this.diem = diem;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }
    
    public void inThongTin()
    {
        System.out.printf("%s%f"," ",diem);
    }
}
