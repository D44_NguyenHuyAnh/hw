/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan4;

/**
 *
 * @author KING
 */
public class SuperCow extends Cow implements Fly,Fight
{
    public SuperCow(String sound) {
        super(sound);
    }
    
    @Override
    public int maxHeight(){        
        return 1000;
    }
    
    public void whatCanDo(){
        super.makeASound();
        System.out.printf("Flying up to %d kms\n",maxHeight());
        System.out.printf("Fighting up to %d rivals\n",maxHeight());
    }

    @Override
    public int rival() {
        return 234;
    }
    
}
