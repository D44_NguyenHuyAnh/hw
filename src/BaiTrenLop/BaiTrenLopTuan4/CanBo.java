/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan4;

/**
 *
 * @author KING
 */
public class CanBo extends Nguoi{
    private String capHam;
    private String chucVu;
    
    public CanBo(){
    }

    public CanBo( String hoTen, boolean gioiTinh, String queQuan,String capHam, String chucVu) {
        super(hoTen, gioiTinh, queQuan);
        this.capHam = capHam;
        this.chucVu = chucVu;
    }

    public String getCapHam() {
        return capHam;
    }

    public void setCapHam(String capHam) {
        this.capHam = capHam;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }
    
    public void inThongTin()
    {
        System.out.printf("%s%s"," "+capHam," "+chucVu);
    }
    
}
