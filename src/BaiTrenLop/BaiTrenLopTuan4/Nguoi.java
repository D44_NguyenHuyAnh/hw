/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan4;

/**
 *
 * @author KING
 */
public class Nguoi {
    private String hoTen;
    private boolean gioiTinh;
    private String queQuan;

    public Nguoi(String hoTen, boolean gioiTinh, String queQuan) {
        this.hoTen = hoTen;
        this.gioiTinh = gioiTinh;
        this.queQuan = queQuan;
    }

    public Nguoi() {
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setGioiTinh(boolean gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public String getHoTen() {
        return hoTen;
    }

    public boolean isGioiTinh() {
        return gioiTinh;
    }

    public String getQueQuan() {
        return queQuan;
    }
    
    public void inThongTin()
    {
        System.out.printf("%s%b%s",hoTen+" ",gioiTinh," "+queQuan);
    }
    
}
