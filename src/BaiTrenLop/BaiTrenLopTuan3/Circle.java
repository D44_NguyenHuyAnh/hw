/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan3;

/**
 *
 * @author KING
 */
public class Circle {
    private double radius;
    private double xPosition;
    private double yPosition;

    public double getRadius() {
        return radius;
    }

    public double getxPosition() {
        return xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setxPosition(double xPosition) {
        this.xPosition = xPosition;
    }

    public void setyPosition(double yPosition) {
        this.yPosition = yPosition;
    }

    public Circle(double radius, double xPosition, double yPosition) {
        this.radius = radius;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }


    
    public double distanceFrom(Circle test)
    {
        return(Math.sqrt(Math.pow(getxPosition()-test.getxPosition(),2)+Math.pow(getyPosition()-test.getyPosition(),2)));
    }

    
}
