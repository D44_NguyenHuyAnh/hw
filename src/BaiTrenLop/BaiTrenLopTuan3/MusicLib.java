/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BaiTrenLop.BaiTrenLopTuan3;

/**
 *
 * @author KING
 */
    public class MusicLib{
        private Song[] Lib=new Song [100];
        private int numberSong = 0;

        public int getNumberSong() {
            return numberSong;
        }

        public void add(Song newSong) {
            if(numberSong<Lib.length)
            {
                Lib[numberSong]=newSong;
                numberSong++;
            }
            else System.out.println("Mang Day !");
        }
        
        public void display(){
            for(int i=0;i<getNumberSong();i++)
                System.out.println(Lib[i].getCode()+" "+Lib[i].getTitle()+" "+Lib[i].getArtist()+" "+Lib[i].getPrice());
        }
    }